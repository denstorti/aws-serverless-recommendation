# Problem Space

**Problem**: As a user, I don't get relevant recommendations about movies and I am missing out movies that interest me

**Solution**: Recommendation system for movies to improve UX, increase retention of paying customers

# User Story mapping

- As a user, I want to get relevant movie recommendations so I can rent/purchase what interests me without wasting time browsing the catalog
- As a user, I like to interact with the platform using my mobile so it I can use it anywhere

### Example

> Actually managed in a Gitlab board.

User interacting with mobile application scenarios: 

|GIVEN	| WHEN | THEN	|
|-------|------|------|
| I want a list of movies recommended to me |  input some keywords | a list of movies associated with that keyword is presented |
|  I want a list of movies recommended to me  | A list of recommended movies | Rank the items in order to recommend the most relevant items first |
| I want a list of movies recommended to me  | A list of recommended movies | Recommend only items the user hasn’t watched (or purchased)|


- Data source: https://datasets.imdbws.com/?opt_id=oeu1604777220253r0.6338255087722044
- Data set: title.basics.tsv.gz

