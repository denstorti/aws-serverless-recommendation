# AWS Serverless Simple Recommendation system

Build a "Netflix Style" Recommendation Engine with Amazon SageMaker using serverless services.



|This is part of the #CloudGuruChallenge||
|---|---|
Topic | 			Machine Learning on AWS
Creator |			Kesha Williams
Goal|				Build a Netflix Style Recommendation Engine with Amazon SageMaker
Outcome |		Gain real machine learning -AWS
Link | https://acloudguru.com/blog/engineering/cloudguruchallenge-machine-learnin

# Prerequisites

- Docker and docker-compose on the deployment machine
- Make
- AWS credentials configured

# How to run

1. make deploy
